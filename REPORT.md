# Weixin Wall
Qifan Lu - 2014013423 - lqf.1996121@gmail.com

## Basic Requirements
* 4 Weixin messages are displayed on the screen at the same time.
* User nickname, avatar and message content are shown.
* A loading image will be shown for each avatar until it's fully loaded.
* History messages are diplayed when the page is refreshed or first opened.  
  (History messages are loaded using `$.get` which wraps primitive AJAX API)
* Old message is removed when new message is added.  
  (Subsequent messages are received using Socket.io)
* A pseudo notice is rolling displayed on top of the page.  
  (The notice can be changed if a backend notice API is provided)
* Adaptive design: the page is carefully designed so that it fits into screen with different sizes.
* Modern-like UI with Segoe UI font from Microsoft.  
  Messages are rolling displayed, regardless of their length.
* Animation makes message appearance and disappearance smoothly.
* Admin messages are shown on the right side of the page, different from normal messages.  
  Each admin messages are shown for 10 seconds.  
  If a new admin message arrives before old message destroys, the old message will be replaced with no animation and the time will be count down from 10 seconds again.  
* Two-way data binding (provided by Angular 2) is used in this project though it is not suitable.

## Project Details
* Project repository: https://coding.net/u/lqf96/p/wechat-wall/git
  Project demo: https://lqf96.coding.me/wechat-wall/
* UI design:
  - Pseudo public notice: Top of the screen
  - Normal messages: Middle of the screen (4 messages)
  - Admin message: Toggled from the right side
  - About region: Bottom of the screen
* 3rd-party libraries used:
  - `jQuery` @ 3.0.0
  - `Angular 2` @ 2.0.0-rc.4
  - `core-js` @ 2.4.0
  - `reflect-metadata` @ 0.1.3
  - `rxjs` @ 5.0.0-beta.6
  - `systemjs` @ 0.19.31
  - `zone.js` @ 0.6.12
  - `socket.io-client` @ 1.4.8
* Backend APIs:
  - Socket.io Endpoint: `https://wall.cgcgbcbc.com`
  - History API: `/api/messages?num=4`
* Project structure
  - Source code (`js/*.ts`) is written in Typescript (a superset of Javascript) and is transpiled to Javascript (`js/*.js`) with `tsc` and `atom-typescript` from Microsoft.
  - `index.ts`: Project entry.
  - `wx-wall.component.ts`: Root Angular 2 web component, which holds every element shown on the web page.
  - `wx-wall.service.ts`: Weixin Wall messages service, which provides messages to components in this project.
  - `msg.component.ts`: Normal Weixin message component that displays a single normal Weixin message.
  - `msg-transition.directive.ts`: Handles message appearance and disappearance animation.
  - `systemjs.config.js`: SystemJS module loader configurations.

## Problems, thoughts and suggestions
* Buggy message animation
  - According to original design, when a message comes out the message region expands and then the message gradually appears, during which the opacity is increased to 1. Similarly before a message is destroyed it disappears and when opacity reaches 0 the message region shrinks to zero size.
  - In this project the above-mentioned animation is encapsulated into `MsgTransitionDirective` in `js/msg-transition.directive.ts` and by applying `msgTransition` and `msgShow` property to tags animation can be added to DOM elements.
  - This works quite well for admin messages, which acts smoothly during animation process, but doesn't work very well for normal Weixin messages. This is because when message transition directive is applyed to a DOM element it hides the element with `width: 0` or `height: 0` CSS directive, but Angular 2 display normal message with its initial width or height, which is not zero. This causes the transition animation not to work properly and normal message appears abruptly.
  - This problem is hard to solve as Angular 2 provides few ways to manipulate DOM and there is no way to alter initial element width or height.
* Restrictions and bugs of Angular 2
  - Angular 2 is still in release candidate phase, so it has a lot of restrictions and bugs.
  - Angular's built-in animation module use automata to represent transition between different CSS states, yet it's not possible to apply animations to host elements and the animation module is hard to use and sometimes buggy.
  - To manipulate DOM in Angular 2 you can only use `Directive`, and you cannot apply directive to host element using `directives` and `HostBinding`.
  - Changes in binded data sometimes cause animation of elements associated to be reset.
* Two-way binding: an awkward approach
  - Two-way binding is a useful feature in front-end programming as it associates UI elements with data, and for CRUD applications two-way binding is an ideal choice.
  - For situations that you want to have more control over DOM elements (like this project), two-way binding is not suitable and may cause problems. For example, we may not be able to tell if existing message elements are reused or created again if the message array is updated, which affects how we handle messages animations.
  - Besides, using two-way binding to separate logic and representation seems like an overkill. Traditionally its painful to write code that creates DOM elements, sets properties and shows elements on the page. But with React and JSX all these process becomes easy and the code is much clearer and can easily reflects its own purpose.
