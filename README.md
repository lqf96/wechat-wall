# [Wechat Wall](https://lqf96.coding.me/wechat-wall/)
\[Archived\] (WFT Course Assignment 4)  
An online Wechat Wall that anyone can send messages to.  
See original report [here](REPORT.md).

## Build & Run
* All dependencies of this project have been pulled to local but no development dependencies are installed.  
* To install development tools run `npm install`.  
* To recompile Typescript code run `npm run tsc`.
* To start up the demo run `npm start`. Alternatively you can run `python -m SimpleHTTPServer` if you do not wish to pull development dependencies.

## License
This project is licensed under [ISC license](LICENSE).
