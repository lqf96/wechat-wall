"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var $ = require("jquery");
exports.ANIMATION_TIME = 1000;
var MsgTransitionDirective = (function () {
    function MsgTransitionDirective(el) {
        this._active = null;
        this.actionBeforeInit = null;
        this.el = $(el.nativeElement);
    }
    Object.defineProperty(MsgTransitionDirective.prototype, "show", {
        set: function (active) {
            if (this._active == null)
                this.actionBeforeInit = active;
            else
                this.doTransition(active);
        },
        enumerable: true,
        configurable: true
    });
    MsgTransitionDirective.prototype.doTransition = function (active) {
        var _this = this;
        var el = this.el;
        if (active == this._active)
            return;
        if (active) {
            el.removeClass("wx-wall-force-zero-" + this.dimensionProp);
            setTimeout(function () {
                el.css({
                    "width": "",
                    "margin": "",
                    "padding": ""
                });
                el.children().css("display", "");
                el.animate({ "opacity": 1 }, exports.ANIMATION_TIME, function () {
                    _this._active = active;
                });
            }, exports.ANIMATION_TIME);
        }
        else
            el.animate({ "opacity": 0 }, function () {
                el.css((_a = {},
                    _a[_this.dimensionProp] = el[_this.dimensionProp]() + "px",
                    _a["margin"] = 0,
                    _a["padding"] = 0,
                    _a
                ));
                el.children().css("display", "none");
                el.addClass("wx-wall-force-zero-" + _this.dimensionProp);
                setTimeout(function () {
                    _this._active = active;
                }, exports.ANIMATION_TIME);
                var _a;
            });
    };
    MsgTransitionDirective.prototype.ngOnInit = function () {
        var el = this.el;
        el.children().css("display", "none");
        el.css({
            "opacity": 0,
            "margin": 0,
            "padding": 0,
            "transition": this.dimensionProp + " " + exports.ANIMATION_TIME + "ms"
        });
        el.addClass("wx-wall-force-zero-" + this.dimensionProp);
        this._active = false;
        if (this.actionBeforeInit != null)
            this.doTransition(this.actionBeforeInit);
    };
    __decorate([
        core_1.Input("msgTransition"), 
        __metadata('design:type', String)
    ], MsgTransitionDirective.prototype, "dimensionProp", void 0);
    __decorate([
        core_1.Input("msgShow"), 
        __metadata('design:type', Boolean), 
        __metadata('design:paramtypes', [Boolean])
    ], MsgTransitionDirective.prototype, "show", null);
    MsgTransitionDirective = __decorate([
        core_1.Directive({
            selector: "[msgTransition]"
        }), 
        __metadata('design:paramtypes', [core_1.ElementRef])
    ], MsgTransitionDirective);
    return MsgTransitionDirective;
}());
exports.MsgTransitionDirective = MsgTransitionDirective;
//# sourceMappingURL=msg-transition.directive.js.map