//SystemJS configuration
(function(global)
{   //Module mappings
    var map = {
        "wx-wall": "js",
        "rxjs": "https://unpkg.com/rxjs@5.0.0-beta.6",
        "jquery": "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js",
        "socket.io-client": "https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.4.8/socket.io.min.js"
    };

    //Package description
    var packages = {
        "wx-wall": {"main": "index.js", "defaultExtension": "js"},
        "rxjs": {"main": "Rx.js", "defaultExtension": "js"}
    };

    //Angular 2 packages
    var ng_pkgs = [
        "common",
        "compiler",
        "core",
        "platform-browser",
        "platform-browser-dynamic"
    ];

    //Individual files
    function pack_index(pkg_name)
    {   map["@angular/"+pkg_name] = "https://unpkg.com/@angular/"+pkg_name+"@2.0.0-rc.4";
        packages["@angular/"+pkg_name] = {"main": "index.js", "defaultExtension": "js"};
    }
    //Bundled
    function pack_umd(pkg_name)
    {   map["@angular/"+pkg_name] = "https://unpkg.com/@angular/"+pkg_name+"@2.0.0-rc.4";
        packages["@angular/"+pkg_name] = {"main": "/bundles/"+pkg_name+".umd.js", "defaultExtension": "js"};
    }

    //Most environments should use UMD; some (Karma) need the individual index files
    var set_pkg_cfg = System.packageWithIndex?pack_index:pack_umd;
    //Add package entries for angular packages
    ng_pkgs.forEach(set_pkg_cfg);

    //Do configuration
    System.config({
        "map": map,
        "packages": packages
    });
})(this);
