"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var Observable_1 = require("rxjs/Observable");
var IO = require("socket.io-client");
var $ = require("jquery");
var BACKEND_ADDR = "https://wall.cgcgbcbc.com", HISTORY_PATH = "/api/messages?num=4";
var WxWallService = (function () {
    function WxWallService() {
        var socket = IO(BACKEND_ADDR);
        var msgObservers = [];
        var adminMsgObservers = [];
        this.msgStream = new Observable_1.Observable(function (o) { msgObservers.push(o); });
        this.adminMsgStream = new Observable_1.Observable(function (o) { adminMsgObservers.push(o); });
        socket.on("new message", function (data) {
            for (var _i = 0, msgObservers_1 = msgObservers; _i < msgObservers_1.length; _i++) {
                var o = msgObservers_1[_i];
                o.next(data);
            }
        });
        socket.on("admin", function (data) {
            for (var _i = 0, adminMsgObservers_1 = adminMsgObservers; _i < adminMsgObservers_1.length; _i++) {
                var o = adminMsgObservers_1[_i];
                o.next(data);
            }
        });
        $.get(BACKEND_ADDR + HISTORY_PATH).then(function (msgs) {
            msgs = msgs.reverse();
            for (var _i = 0, msgObservers_2 = msgObservers; _i < msgObservers_2.length; _i++) {
                var o = msgObservers_2[_i];
                for (var _a = 0, msgs_1 = msgs; _a < msgs_1.length; _a++) {
                    var msg = msgs_1[_a];
                    o.next(msg);
                }
            }
        });
    }
    WxWallService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], WxWallService);
    return WxWallService;
}());
exports.WxWallService = WxWallService;
//# sourceMappingURL=wx-wall.service.js.map