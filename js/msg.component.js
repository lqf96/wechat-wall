"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var $ = require("jquery");
var MSG_COLORS = [
    "blue",
    "purple",
    "red",
    "green"
];
var currentColorIndex = 0;
var LOADING_IMG_URL = "assets/loading.png";
$("<img />").attr("src", LOADING_IMG_URL);
var MsgComponent = (function () {
    function MsgComponent() {
    }
    Object.defineProperty(MsgComponent.prototype, "msg", {
        set: function (newMsg) {
            var _this = this;
            var headimgurl = newMsg.headimgurl;
            newMsg.headimgurl = LOADING_IMG_URL;
            this._msg = newMsg;
            $("<img />").attr("src", headimgurl)
                .on("load", function () {
                _this._msg.headimgurl = headimgurl;
            });
        },
        enumerable: true,
        configurable: true
    });
    MsgComponent.prototype.ngOnInit = function () {
        this.bgColor = MSG_COLORS[currentColorIndex % MSG_COLORS.length];
        currentColorIndex++;
    };
    __decorate([
        core_1.HostBinding("style.background-color"), 
        __metadata('design:type', String)
    ], MsgComponent.prototype, "bgColor", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object), 
        __metadata('design:paramtypes', [Object])
    ], MsgComponent.prototype, "msg", null);
    MsgComponent = __decorate([
        core_1.Component({
            selector: "wx-wall-msg",
            templateUrl: "html/msg.component.html",
            styleUrls: ["css/msg.component.css"]
        }), 
        __metadata('design:paramtypes', [])
    ], MsgComponent);
    return MsgComponent;
}());
exports.MsgComponent = MsgComponent;
//# sourceMappingURL=msg.component.js.map