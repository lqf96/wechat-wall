import {Directive, ElementRef, Input, OnInit} from "@angular/core";
import $ = require("jquery");

//Animation time
export const ANIMATION_TIME = 1000;

@Directive({
    selector: "[msgTransition]"
})
export class MsgTransitionDirective implements OnInit
{   //jQuery element
    private el: JQuery;
    //Last animation
    private _active: boolean = null;
    //Direction ("width" by default)
    @Input("msgTransition") dimensionProp: string;
    //Action before initialization
    private actionBeforeInit: boolean = null;

    //Show / hide property
    @Input("msgShow")
    set show(active: boolean)
    {   //Not initialized
        if (this._active==null)
            this.actionBeforeInit = active;
        //Initialized
        else
            this.doTransition(active);
    }

    //Do transition
    private doTransition(active: boolean)
    {   //Element
        let el = this.el;
        //Not changed
        if (active==this._active)
            return;

        //Show element
        if (active)
        {   el.removeClass("wx-wall-force-zero-"+this.dimensionProp);
            setTimeout(() => {
                //Handle host element CSS
                el.css({
                    "width": "",
                    "margin": "",
                    "padding": ""
                });
                //Make children element visible
                el.children().css("display", "");
                //Make element opaque
                el.animate({"opacity": 1}, ANIMATION_TIME, () => {
                    this._active = active;
                });
            }, ANIMATION_TIME);
        }
        //Hide element
        else
            el.animate({"opacity": 0}, () => {
                //Handle host element CSS
                el.css({
                    [this.dimensionProp]: el[this.dimensionProp]()+"px",
                    "margin": 0,
                    "padding": 0
                });
                //Make children element invisible
                el.children().css("display", "none");
                //Set size to zero
                el.addClass("wx-wall-force-zero-"+this.dimensionProp);
                setTimeout(() => {
                    this._active = active;
                }, ANIMATION_TIME);
            });
    }

    //Constructor
    constructor(el: ElementRef)
    {   //jQuery element
        this.el = $(el.nativeElement);
    }

    //On init
    ngOnInit()
    {   let el = this.el;

        //Make children invisible
        el.children().css("display", "none");
        //Hide element
        el.css({
            "opacity": 0,
            "margin": 0,
            "padding": 0,
            "transition": `${this.dimensionProp} ${ANIMATION_TIME}ms`
        });
        //Apply zero width class
        el.addClass("wx-wall-force-zero-"+this.dimensionProp);

        this._active = false;
        if (this.actionBeforeInit!=null)
            this.doTransition(this.actionBeforeInit);
    }
}
