//Imports
import {Component, Input, HostBinding, ElementRef, OnInit} from "@angular/core";
import {Observer, Observable} from "rxjs";
import $ = require("jquery");
import {WxMsg} from "./wx-wall.service";

//Colors
const MSG_COLORS = [
    "blue",
    "purple",
    "red",
    "green"
];

//Current color index
let currentColorIndex = 0;

//Loading image URL
const LOADING_IMG_URL = "assets/loading.png";
//Load loading image in advance
$("<img />").attr("src", LOADING_IMG_URL);

//Message component
@Component({
    selector: "wx-wall-msg",
    templateUrl: "html/msg.component.html",
    styleUrls: ["css/msg.component.css"]
})
export class MsgComponent implements OnInit
{   //Message
    _msg: WxMsg;
    //Background color
    @HostBinding("style.background-color") bgColor: string;

    //Set message
    @Input()
    set msg(newMsg: WxMsg)
    {   //Show message and store image URL
        let headimgurl = newMsg.headimgurl;
        newMsg.headimgurl = LOADING_IMG_URL;
        this._msg = newMsg;
        //Show loading image
        $("<img />").attr("src", headimgurl)
            .on("load", () => {
                this._msg.headimgurl = headimgurl;
            });
    }

    //On init
    ngOnInit()
    {   //Set background color
        this.bgColor = MSG_COLORS[currentColorIndex%MSG_COLORS.length];
        currentColorIndex++;
    }
}
