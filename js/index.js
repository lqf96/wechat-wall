"use strict";
var core_1 = require("@angular/core");
var platform_browser_dynamic_1 = require("@angular/platform-browser-dynamic");
var wx_wall_component_1 = require("./wx-wall.component");
core_1.enableProdMode();
platform_browser_dynamic_1.bootstrap(wx_wall_component_1.WxWallComponent);
//# sourceMappingURL=index.js.map