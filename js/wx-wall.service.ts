//Imports
import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {Observer} from "rxjs/Observer";
import IO = require("socket.io-client");
import $ = require("jquery");

//Constants
const BACKEND_ADDR = "https://wall.cgcgbcbc.com",
    HISTORY_PATH = "/api/messages?num=4";

//Weixin wall message
export interface WxMsg
{   //Nickname
    nickname: string;
    //Content
    content: string;
    //Head image URL
    headimgurl?: string;

    //Animation state (used for normal message)
    animationState?: boolean;
}

//Weixin wall message service
@Injectable()
export class WxWallService
{   //Message stream
    public msgStream: Observable<WxMsg>;
    //Admin message stream
    public adminMsgStream: Observable<WxMsg>;

    //Constructor
    constructor()
    {   let socket = IO(BACKEND_ADDR);
        let msgObservers: Observer<WxMsg>[] = [];
        let adminMsgObservers: Observer<WxMsg>[] = [];

        this.msgStream = new Observable<WxMsg>((o: Observer<WxMsg>) => { msgObservers.push(o); });
        this.adminMsgStream = new Observable<WxMsg>((o: Observer<WxMsg>) => { adminMsgObservers.push(o); });
        //Socket.io event
        socket.on("new message", (data: WxMsg) => {
            for (let o of msgObservers)
                o.next(data);
        });
        socket.on("admin", (data: WxMsg) => {
            for (let o of adminMsgObservers)
                o.next(data);
        });

        //History messages
        $.get(BACKEND_ADDR+HISTORY_PATH).then((msgs: WxMsg[]) => {
            msgs = msgs.reverse();
            for (let o of msgObservers)
                for (let msg of msgs)
                    o.next(msg);
        });
    }
}
