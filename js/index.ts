//Imports
import {enableProdMode} from "@angular/core";
import {bootstrap} from "@angular/platform-browser-dynamic";
import {WxWallComponent} from "./wx-wall.component";

//Enable production mode
enableProdMode();
//Bootstrap components
bootstrap(WxWallComponent);
