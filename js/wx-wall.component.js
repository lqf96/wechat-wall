"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var wx_wall_service_1 = require("./wx-wall.service");
var msg_component_1 = require("./msg.component");
var msg_transition_directive_1 = require("./msg-transition.directive");
var MAX_MSG_COUNT = 4;
var ADMIN_MSG_TIMEOUT = 10000;
var AdminMsgTimer = (function () {
    function AdminMsgTimer(component) {
        this.component = component;
        this.valid = true;
        setTimeout(this.timeoutFunc.bind(this), ADMIN_MSG_TIMEOUT);
    }
    AdminMsgTimer.prototype.timeoutFunc = function () {
        if (this.valid) {
            this.component.adminState = false;
            this.component.lastTimer = null;
        }
    };
    return AdminMsgTimer;
}());
var WxWallComponent = (function () {
    function WxWallComponent(wxSrv) {
        this.wxSrv = wxSrv;
        this.notice = "An example notice on top of the page";
        this.msgs = [];
        this.adminMsg = null;
        this.adminState = false;
        this.lastTimer = null;
    }
    WxWallComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.wxSrv.msgStream.subscribe(function (msg) {
            msg.animationState = true;
            var needPop = _this.msgs.length >= MAX_MSG_COUNT;
            if (needPop) {
                _this.msgs[_this.msgs.length - 1].animationState = false;
                setTimeout(function () {
                    _this.msgs.pop();
                }, 2 * msg_transition_directive_1.ANIMATION_TIME);
            }
            setTimeout(function () {
                _this.msgs.unshift(msg);
            }, needPop ? msg_transition_directive_1.ANIMATION_TIME : 0);
        });
        this.wxSrv.adminMsgStream.subscribe(function (msg) {
            _this.adminMsg = msg;
            if (_this.lastTimer)
                _this.lastTimer.valid = false;
            _this.adminState = true;
            _this.lastTimer = new AdminMsgTimer(_this);
        });
    };
    WxWallComponent = __decorate([
        core_1.Component({
            selector: "#wx-wall",
            templateUrl: "html/wx-wall.component.html",
            providers: [wx_wall_service_1.WxWallService],
            styleUrls: ["css/wx-wall.component.css"],
            directives: [msg_component_1.MsgComponent, msg_transition_directive_1.MsgTransitionDirective]
        }), 
        __metadata('design:paramtypes', [wx_wall_service_1.WxWallService])
    ], WxWallComponent);
    return WxWallComponent;
}());
exports.WxWallComponent = WxWallComponent;
//# sourceMappingURL=wx-wall.component.js.map