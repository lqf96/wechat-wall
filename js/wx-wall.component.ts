//Imports
import {Component, OnInit} from "@angular/core";
import $ = require("jquery");
import {WxWallService, WxMsg} from "./wx-wall.service";
import {MsgComponent} from "./msg.component";
import {ANIMATION_TIME, MsgTransitionDirective} from "./msg-transition.directive";

//Maximum message count
const MAX_MSG_COUNT = 4;
//Admin message timeout
const ADMIN_MSG_TIMEOUT = 10000;

//Admin message timer
class AdminMsgTimer
{   //Valid
    valid: boolean = true;

    //Constructor
    constructor(private component: WxWallComponent)
    {   setTimeout(this.timeoutFunc.bind(this), ADMIN_MSG_TIMEOUT);
    }

    //Timeout function
    timeoutFunc()
    {   if (this.valid)
        {   this.component.adminState = false;
            this.component.lastTimer = null;
        }
    }
}

//Main component
@Component({
    selector: "#wx-wall",
    templateUrl: "html/wx-wall.component.html",
    providers: [WxWallService],
    styleUrls: ["css/wx-wall.component.css"],
    directives: [MsgComponent, MsgTransitionDirective]
})
export class WxWallComponent implements OnInit
{   //Notice
    notice = "An example notice on top of the page";

    //Messages
    msgs: WxMsg[] = [];

    //Admin message
    adminMsg: WxMsg = null;
    //Animation state
    adminState = false;
    //Last timeout function
    lastTimer: AdminMsgTimer = null;

    //Constructor
    constructor(public wxSrv: WxWallService) {}

    //On init
    ngOnInit()
    {   //Subscribe to message stream
        this.wxSrv.msgStream.subscribe((msg: WxMsg) => {
            //Animation state
            msg.animationState = true;
            //Pop old message when needed
            const needPop = this.msgs.length>=MAX_MSG_COUNT;
            if (needPop)
            {   this.msgs[this.msgs.length-1].animationState = false;
                setTimeout(() => {
                    this.msgs.pop();
                }, 2*ANIMATION_TIME);
            }
            //Insert new message
            setTimeout(() => {
                this.msgs.unshift(msg);
            }, needPop?ANIMATION_TIME:0);
        });

        //Subscribe to administrator message stream
        this.wxSrv.adminMsgStream.subscribe((msg: WxMsg) => {
            this.adminMsg = msg;
            //Old message still exists
            if (this.lastTimer)
                this.lastTimer.valid = false;
            //Show content
            this.adminState = true;
            //Set a new timeout function
            this.lastTimer = new AdminMsgTimer(this);
        });
    }
}
